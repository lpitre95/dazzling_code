//remember, this is a comment
//this code will turn a LED on when a button is pushed

//let's create some variables
//variables contain information, for example:
int led = 3;//the name of the variable is "led", and it stores the number 3
//the "int" before "led" means that we are going to store a number(this type of number is an integer)
//now let's make a variable that will store the pin number of the button
int button = 8;


//the setup function
//setup() runs once at the beginning of the program, when the Arduino is powered On.
void setup(){
	pinMode(led, OUTPUT);//set the pin called led as an OUTPUT...since led contains 3, this will set pin 3
	pinMode(button, INPUT);//set the pin called button as an INPUT because we want to receive a signal...
	//...since button contains 8, this will set pin 8
}//end setup()

//the loop function
//loop() runs right after setup()
//loop() never stops running as long as the Arduino is powered On.
void loop(){
	//now we have to "sense" if our button is ever pressed:
	//create a variable that contains the "state" of our button
	int state = digitalRead(button);
	//now we have to make a decision:
	//if our button is pressed, then we turn the LED on
	//if our button is not pressed, then the LED will turn off
	if(state == HIGH){//two equal signs means that we are testing if the things on either side are equal or not
//one equal sign is for assignment, like creating a variable and setting it something like a number.
		//if the button is pressed, then do is:
		digitalWrite(led, HIGH);//use led(pin 3), send it a command to turn HIGH, HIGH means on
	}else{//if the button is not pressed do this:
		digitalWrite(led, LOW);//use led(pin 3), send it a command to turn LOW, LOW means off
	}
}//end loop()