//this is a comment, it does not affect code
//these are meant for people to read
//you need to leave notes that tell other people what your code is actually doing

//the setup function sets the modes of the pins we are using
void setup(){
	pinMode(3, OUTPUT);//set pin 3 as an output
}//end setup

//the loop function runs after the setup function
//the loop function will run the code inside of the braces so long as the Arduino is on!
void loop(){
	digitalWrite(3, HIGH);//use pin 3, send it a command to tell it to turn on, HIGH means on
	delay(1000);//delay the program for 1000 milli-seconds, meaning wait for 1 second
	digitalWrite(3, LOW);//use pin 3, send it a command to tell it to turn off, LOW means off
	delay(1000);//delay the program for 1000 milli-seconds, meaning wait for 1 second
}