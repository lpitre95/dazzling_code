/**
*
*Author: Luc Pitre<pitreluc@gmail.com>
*Created On: 15 April 2015
*
*/
//including the library to use a servo
#include<Servo.h>
//including library for the ultrasonic sensor
#include<NewPing.h>

//define the pins used for the ultrasonic sensor
#define TRIG_PIN 12//the trigger pin from the sensor is connected to digital pin 12
#define ECHO_PIN 11//the echo pin from the ultrasonic sensor is connected to digital pin 11
#define MAX_DIST 200

//create a servo object
Servo servo;
//create an object that represents the ultrasonic sensor
NewPing sensor(TRIG_PIN, ECHO_PIN, MAX_DIST);

//function prototypes
void sweep(int);
//void say(string);
void launch(int);
void startPosition(int);
void ledOn(int);
void ledOff(int);
void checkGoal(void);

int time = 5;//how quickly we want the servo to turn
int button = 5;
int led = 13;


void setup(){
  servo.attach(9);
  pinMode(button, INPUT);
  pinMode(led, OUTPUT);
  Serial.begin(115200);
  startPosition(time);
}//end setup

void loop(){
  delay(10);
  int reading = digitalRead(button);
  if(reading == HIGH){
    launch(time);
    delay(100);
  }else if (reading == LOW){
    Serial.println("button not pressed");
    delay(10);
  }else{
    Serial.println("error!");
    delay(10);
  }//end else-if
  delay(100);
  
  
}//end loop


void launch(int t){
  Serial.println("READY");
  delay(100);
  servo.write(5);
  Serial.println("LAUNCH!");
  delay(100);
  for(int i = 0; i < 175; i++){
    servo.write(i);
  }
  delay(t);
  checkGoal();
  ledOff(led);
}//end launch

void startPosition(int t){
 Serial.println("Start position");
 delay(t);
 for(int i = 175; i > 50; i--){
   servo.write(i);
 }//end for
}

void ledOn(int led){
  digitalWrite(led, HIGH);
  delay(200);
  Serial.println("LED ON");
}

void ledOff(int led){
  digitalWrite(led, LOW);
  delay(200);
  Serial.println("LED OFF");
}

void checkGoal(){
  delay(20);
  unsigned int uS = sensor.ping();
  Serial.println("PING!: ");
  int dist = uS/US_ROUNDTRIP_CM;
  Serial.println(dist);
  if(dist <= 15){
    ledOn(led);
    Serial.println("SCORE!");
  }else{
   ledOff(led); 
   Serial.println("no score");
 }
   
  delay(10);
}//end checkGoal()

